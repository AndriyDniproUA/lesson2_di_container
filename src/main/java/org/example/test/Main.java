package org.example.test;

import org.example.test.container.ApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext ctx = new ApplicationContext("org.example.test");
        UserService userService = ctx.getBean(UserService.class);
        userService.showUsers();
    }
}
