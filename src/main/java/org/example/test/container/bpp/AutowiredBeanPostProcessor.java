package org.example.test.container.bpp;

import org.example.test.container.ApplicationContext;
import org.example.test.container.BeanPostProcessor;
import org.example.test.container.annotations.Autowired;
import org.example.test.container.annotations.Component;

import java.util.Arrays;

@Component
public class AutowiredBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object processBeforeInitialization(Object bean, ApplicationContext context) {
        Class<?> beanClass = bean.getClass();
        Arrays.stream(beanClass.getDeclaredFields())
                .filter(p -> p.isAnnotationPresent(Autowired.class))
                .peek(p -> p.setAccessible(true))
                .forEach(p -> {
                    Object value = context.getBean(p.getType());
                    try {
                        p.set(bean,value);
                    } catch (IllegalAccessException e) { }
                });
        return bean;
    }
}
