package org.example.test.container.utils;

import lombok.SneakyThrows;

import java.io.File;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class PackageScanner {

    @SneakyThrows
    public List<Class<?>> scanPackage(String packageName){
        ClassLoader classLoader = getClass().getClassLoader();
        URL packageUrl = classLoader.getResource(packageName.replace(".","/"));
        File packageDir = new File(packageUrl.toURI());

        List<Class<?>> classes = new LinkedList<>();

        for (File file : packageDir.listFiles()) {
            if (file.isDirectory()){
                List <Class<?>> inner = scanPackage(packageName+"."+file.getName());
                classes.addAll(inner);

            } else if (file.isFile()) {
                getClassByFileName(file.getName(), packageName).ifPresent(classes::add);
            }
        }
        return classes;
    }

    private Optional<Class<?>> getClassByFileName(String name, String packageName) {
        if (!name.endsWith(".class")) {
            return Optional.empty();
        }
        String className = name.substring(0,name.length()-6);
        className = packageName+"."+className;

        try {
            return Optional.of(getClass().getClassLoader().loadClass(className));
        } catch (ClassNotFoundException e) {
            return Optional.empty();
        }
    }
}
